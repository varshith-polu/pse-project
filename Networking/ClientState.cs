﻿// -----------------------------------------------------------------------
// <author> 
//      Ayush Mittal
// </author>
//
// <date> 
//      12-10-2018 
// </date>
// 
// <reviewer>
//      Libin N George
//      Rajat Sharma
// </reviewer>
//
// <copyright file="ClientState.cs" company="B'15, IIT Palakkad">
//      This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
//
// <summary>
//      This class represents the state structure/object used in asynchronous networking calls.
// </summary>
// -----------------------------------------------------------------------

namespace Masti.Networking
{
    using System.Net;
    using System.Net.Sockets;

    /// <summary>
    /// This class represents the state structure/object used in asynchronous networking calls.
    /// </summary>
    public class ClientState
    {
        /// <summary>
        /// Gets or sets Receive buffer.
        /// </summary>
        private byte[] dataToSend;

        /// <summary>
        /// Gets or sets the client socket.
        /// </summary>
        public Socket Socket { get; set; } = null;

        /// <summary>
        /// Gets or sets number of tries already made to the attached data.
        /// </summary>
        public int Retries { get; set; } = 0;

        /// <summary>
        /// Gets or sets amount Of Data Sent
        /// </summary>
        public int DataSent { get; set; } = 0;

        /// <summary>
        /// Gets or sets the actual message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets the ip from the socket, It can happen that socket is null in which case it returns 0.0.0.0.
        /// </summary>
        public string IP
        {
            get
            {
                if (this.Socket == null)
                {
                    return "0.0.0.0";
                }
                else
                {
                    return ((IPEndPoint)this.Socket.RemoteEndPoint).Address.ToString();
                }
            }
        }

        /// <summary>
        /// Sets the private dataToSend Attribute since property can't return arrays
        /// </summary>
        /// <param name="input">Input Array</param>
        public void SetDataToSend(byte[] input)
        {
            this.dataToSend = input;
        }

        /// <summary>
        /// Gets the private dataToSend Attribute since property can't return arrays.
        /// </summary>
        /// <returns>Current Value of dataToSend</returns>
        public byte[] DataToSend()
        {
            return this.dataToSend;
        }
    }
}